import AsyncStorage from '@react-native-async-storage/async-storage';

//https://stackoverflow.com/questions/43695654/right-way-to-setitem-in-asyncstorage

//dodawanie gry do tablicy
export async function setGame(date, playerOne,playerTwo,playerOneWon){
  addDataAsync("games",{ date, playerOne, playerTwo, playerOneWon});
}

export async function getGames(date, playerOne,playerTwo,playerOneWon){
  getDataAsync("games");
}

export async function getDataAsync(key){
    try {
        const value = await AsyncStorage.getItem(key);
        return value != null ? JSON.parse(value) : null;
    } catch(e) {
        console.log("Error: " + e);
    }
}

export async function addDataAsync(key,data){
    try {
        var value = await AsyncStorage.getItem(key);
        value=JSON.parse(value);
        if(value ==null){
          value = [];
        }
        value.push(data);
        const jsonValue = JSON.stringify(value);
        await AsyncStorage.setItem(key, jsonValue);
    } catch(e) {
        console.log("Error: " + e);
    }
}

export async function dropDataAsync(key){
    AsyncStorage.removeItem(key);
}

const data = [
  { date: '12/1/2011', playerOne: 'Janusz', playerTwo: 'Helena', playerOneWon:true },
  { date: '5/5/2021', playerOne: 'Filip', playerTwo: 'Damian', playerOneWon:false }
];

