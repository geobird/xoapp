import * as React from 'react';
import { Image,Text, View, StyleSheet, ScrollView,SafeAreaView,TouchableOpacity,ImageBackground} from 'react-native';
import {Icon} from 'react-native-elements';
import HeaderItem from '../navigation/HeaderItem';
import XOApp_2 from '../assets/XOApp_2.png';
import AppLoading from 'expo-app-loading';
import _ from 'lodash';
import ImageBackground2 from '../assets/p1_2.jpg';
import LOGO from '../assets/logo_biel_2.png';
import * as Async from '../asyncstorge/AsyncStorge';

class Home extends React.Component {

render(){
  console.log("hello");
  Async.addDataAsync('test',{ date: '5/5/2021', playerOne: 'Filip', playerTwo: 'Damian', playerOneWon:false })
  Async.getDataAsync('test').then(console.log)
  //Async.dropDataAsync('test')    
  
   let {navigation} = this.props; 
    return ( 
      <View style={styles.container}>
      <ImageBackground source={ImageBackground2} style={styles.backgroundStyle}>
        <SafeAreaView style={{marginTop:40}}>
          <HeaderItem navigation={navigation} title={"Home"} />
          <View style={styles.viewStyle} >
           <Image source={LOGO} style={styles.imageStyle}/>
            <TouchableOpacity onPress={() =>  navigation.navigate("Game")}  style={styles.buttonStyle}>
                <Text style={styles.buttonText}>Game</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate("Result")} style={styles.buttonStyle}>
                <Text style={styles.buttonText}>Results</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.navigate("Instruction")} style={styles.buttonStyle}>
              <Text style={styles.buttonText}>Instruction</Text>
            </TouchableOpacity>
          </View>
        </SafeAreaView>
        </ImageBackground>
      </View>
    );
}
  
}


const styles = StyleSheet.create({
  container:{
    flex:1, 
    backgroundColor:'white'
  },
  viewStyle:{
    justifyContent:'center', 
    alignItems:'center', 
    marginTop:50
  },
  buttonStyle:{
    width:200,
    justifyContent:'center', 
    alignItems:'center',
    borderRadius:6,
    backgroundColor:'transparent',
    marginBottom:20,
    marginTop:10
  },

  buttonText:{
    color:'#fff',
    fontSize: 20,
    margin:5,
    fontStyle:'bold'
  },
  imageStyle:{
    width: 200,
    height: 200,
    resizeMode: 'contain',
    marginBottom:40
  },
  backgroundStyle:{
    width:'100%', 
    height:'100%',  
    
  },
 
})
  
export default Home