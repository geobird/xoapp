import * as React from 'react';
import {StyleSheet, Text, TouchableOpacity, View, Animated, Button,TextInput,ImageBackground} from "react-native";
import HeaderItem from '../navigation/HeaderItem';
import _ from 'lodash';
import ImageB from '../assets/tloKK2.png';

class TestNumber extends React.Component {
   
    render() {
        const {navigation} = this.props;
         
        return (
              <ImageBackground source={ImageB} style={styles.backgroundStyle}>
              <View style={{marginTop:40}}>
               <HeaderItem navigation={navigation} title={"TestNumber"} />
               <View style={{alignItems:'center',justifyContent:'center',marginTop:'40%'}}>
                    <Text style={styles.winTextStyle}>Gratulation</Text>
                    <Text style={styles.winTextStyle}>Win O</Text>
                    <TouchableOpacity onPress={() =>  navigation.navigate("Home")} style={styles.buttonStyle }>
                          <Text style={styles.buttonTextStyle}>To Main</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() =>  navigation.navigate("Result")} style={styles.buttonStyle }>
                          <Text style={styles.buttonTextStyle}>To Statistic</Text>
                    </TouchableOpacity>
                    </View>
                    </View>
              </ImageBackground>           
             
        );
    }
}
const styles = StyleSheet.create({
 
    buttonStyle:{
    width:200,
    height:50,
    justifyContent:'center', 
    alignItems:'center',
    borderRadius:10,
    backgroundColor:'#34206A',
    marginTop:40,
  },
  buttonTextStyle:{
    color:'white',
    fontSize:16
  },
  winTextStyle:{
    fontSize:30,
    color:'#34206A',
    textAlign: 'center'

  },
  backgroundStyle:{
    width:'100%', 
    height:'100%',  
  },
 
});

export default TestNumber;