import * as React from 'react';
import { FlatList, RefreshControl, StyleSheet, View,ImageBackground } from 'react-native';
import { Row, Table } from 'react-native-table-component';
import { Card } from 'react-native-paper';
import * as Async from '../asyncstorge/AsyncStorge';
import HeaderItem from '../navigation/HeaderItem';
import xoBackground from './TicTacToe/backgroundCol.png';

class Results extends React.Component {
  state = {
    refreshing: false,
    results: [],
  };

  
  componentDidMount() {
    this.refreshResults();
  }

  refreshResults = () => {
    Async.getDataAsync('game').then((res) =>
      this.setState({
        results: res,
      })
    );
  };
  renderItem = ({ item }) => {
    console.log(item);
    const { date, playerOne, playerTwo, playerOneWon } = item;
    let { winner } = item;
    if (playerOneWon == true) {
      winner = playerOne;
    } else {
      winner = playerTwo;
    }
    return (
      <Row
        data={[date, playerOne, playerTwo, winner]}
        textStyle={{ margin: 6 }}
        borderStyle={{ borderWidth: 1, borderColor: '#944690' }}
      />
    );
  };

  handleOnRefresh = () => {
    this.setState(
      {
        refreshing: true,
      },
      () => {
        this.refreshResults();
        this.setState({ refreshing: false });
      }
    );
  };

  render() {
    console.log('otwarto result');
    let { navigation } = this.props;
    const { refreshing } = this.state;
    return (
      
      <View style={styles.wrapper}>
      <HeaderItem navigation={navigation} title={"Results"}/>
      <ImageBackground
              source={xoBackground}
              style={styles.backgroundStyle}>
        <View style={styles.container}>
          <Table style={styles.table}>
            <Row
              data={['Date', 'playerOne', 'playerTwo', 'Winner']}
              style={styles.HeadStyle}
              textStyle={styles.TableText}
            />
            <FlatList
              renderItem={this.renderItem}
              data={this.state.results}
              keyExtractor={(item, index) => index.toString()}
              refreshControl={
                <RefreshControl
                  refreshing={refreshing}
                  onRefresh={this.handleOnRefresh}
                />
              }
            />
          </Table>
        </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    paddingTop:'10%',
    backgroundColor:'white'
  },

  container: {
    flex: 0.8,
  },

  table: {
    margin: 8,
    flex: 1,
  },

  HeadStyle: {
    height: 50,
    alignContent: 'center',
    backgroundColor: '#944690',
    color: 'white',
    fontWeight: 'bold',
    fontSize: 20,
  },

  TableText: {
    margin: 10,
    color: 'white',
    fontWeight: 'bold',
  },

  backgroundStyle:{
    width:'100%', 
    height:'100%',  
  },
});

export default Results;
