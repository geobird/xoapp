import React, { Component } from 'react';
import * as Async from '../../asyncstorge/AsyncStorge';
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  Modal,
  TouchableHighlight,
  Alert,
  TextInput,
  ImageBackground,
} from 'react-native';
import Board from './Board';
import xoBackground from './backgroundCol.png';


export default class Game extends Component {
  constructor() {
    super();
    this.state = {
      squares: Array(9).fill(null),
      endGame: false,
      xIsNext: true,
      modalVisible: true,
      modalVisible2: true,
      nickX: '',
      nickO: '',
      winner:'',
      date: Date,
    };
  }
 
  handleClick(i) {
    const squares = this.state.squares;
    if (this.state.endGame || squares[i] != null) return;

    if (squares[i] || this.calculateWinner()) {
      return;
    }
    squares[i] = this.state.xIsNext ? 'X' : 'O';
    this.setState({
      squares: squares,
      xIsNext: !this.state.xIsNext,
    });
  }

 setModalVisible(visible) {
    this.setState({ modalVisible: visible });
  }
   setModalVisible2(visible) {
    this.setState({ modalVisible2: visible });
  }

  nickXChange = (nickX) => {
     
    this.setState({
      ...this.state,
      nickX,
    });
    
  };

  nickOChange = (nickO) => {
    this.setState({
      ...this.state,
      nickO,
    });
    console.log({nickO});
  };

  reset() {
    this.setState({
      squares: Array(9).fill(null),
      endGame: false,
      xIsNext: true,
    });
  }

  calculateWinner() {
    const squares = this.state.squares;

    for (let i = 0; i < winLines.length; i++) {
      const [a, b, c] = winLines[i];
      if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {        
        return squares[a];
      }
     
    }

    return null;
  }

  render() {
    const winner = this.calculateWinner();

    let status;
    let player;
    console.log('winner' + {winner})
   if (winner) {

      const object = {
      date: new Date(),
      playerOne: this.state.nickX,
      playerTwo: this.state.nickO,
      playerOneWon: winner == this.state.nickX ? true : false
    }
    console.log(object);
      status = 'Winner: ';
      player = winner;
      Async.addDataAsync("game", object)
      return(
     <View style={{ marginTop: 22 }}>
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible2}>
            <ImageBackground
              source={xoBackground}
              style={styles.backgroundStyle}>
              <View
                style={{
                  marginTop: 150,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View style={{ alignItems: 'center' }}>
                  <View style={styles.textBackground}>
                    <Text style={styles.modalTitle}>Congratulations</Text>
                    <Text style={styles.modalTitle}> {winner} you won!!</Text>
                    <Text style={styles.modalTitle3}>Close activity and use side </Text>
                    <Text style={styles.modalTitle3}>menu to go menu or new Game</Text>
                  </View>
                  <TouchableHighlight
                    onPress={() => {
                      this.setModalVisible2(!this.state.modalVisible2);
                    }}
                    style={styles.startGameButton}>
                    <Text style={styles.buttonText}>Close window</Text>
                  </TouchableHighlight>
                </View>
              </View>
            </ImageBackground>
          </Modal>  
        </View>
    );
    } else {
      status = 'Next player: ';
      player = this.state.xIsNext ? this.state.nickX : this.state.nickO;
      
    }



    return (
      <View>
        <View style={{ marginTop: 22 }}>
          <Modal
            animationType="slide"
            transparent={false}
            visible={this.state.modalVisible}>
            <ImageBackground
              source={xoBackground}
              style={styles.backgroundStyle}>
              <View
                style={{
                  marginTop: 150,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <View style={{ alignItems: 'center' }}>
                  <View style={styles.textBackground}>
                    <Text style={styles.modalTitle}>Give the name</Text>
                    <Text style={styles.modalTitle}>for X and O</Text>
                  </View>
                  <View style={styles.titleGamerBox}>
                    <Text style={styles.titleGamerX}>Gamer X</Text>
                    <Text style={styles.titleGamerO}>Gamer O</Text>
                  </View>
                  <View style={styles.titleGamerBox}>
                    <TextInput
                      value={this.state.nickX}
                      style={styles.inputX}
                      onChangeText={(value) => this.nickXChange(value)}
                      placeholder={'X'}
                    />
                    <TextInput
                      value={this.state.nickO}
                      style={styles.inputO}
                      onChangeText={(value) => this.nickOChange(value)}
                      placeholder={'O'}
                    />
                  </View>
                  <TouchableHighlight
                    onPress={() => {
                      this.setModalVisible(!this.state.modalVisible);
                    }}
                    style={styles.startGameButton}>
                    <Text style={styles.buttonText}>Start the game</Text>
                  </TouchableHighlight>
                </View>
              </View>
            </ImageBackground>
          </Modal>  
        </View>
        <Text style={styles.title}>Tic Tac Toe</Text>
        <Text>
          <Text style={styles.status}>{status}</Text>
          <Text style={styles.textPlayer}>{player}</Text>
        </Text>
        <Board squares={this.state.squares} winLine={this.state.winLine} onClick={(i) => this.handleClick(i)}/>
        <TouchableOpacity onPress={() => this.reset()} style={styles.buttonStyle}>
          <Text style={styles.buttonText}>Press here to reset!!!</Text>
        </TouchableOpacity>
      </View>
      
    );
  }
}

const styles = StyleSheet.create({
  title: {
    marginBottom: 20,
    textAlign: 'center',
    fontSize: 30,
    fontWeight: 'bold',
  },
  status: {
    fontSize: 16,
  },
  textPlayer: {
    fontWeight: 'bold',
    fontSize: 16,
  },
 
  backgroundStyle: {
    width: '100%',
    height: '100%',
  },
  modalTitle: {
    color: '#8D3E8F',
    fontSize: 35,

    fontWeight: 'bold',
  },
   modalTitle3: {
    color: '#8D3E8F',
    fontSize: 20,
    marginTop:20
  },
  textBackground: {
    backgroundColor: '#fff',
    alignItems: 'center',
    textAlign: 'center',
    marginBottom: 120,
  },
  titleGamerBox: {
    flexDirection: 'row',
    backgroundColor: '#fff',
  },
  titleGamerX: {
    fontSize: 25,
    color: '#8D3E8F',
    marginRight: 30,
    marginBottom: 30,
  },
  titleGamerO: {
    fontSize: 25,
    color: '#8D3E8F',
    marginBottom: 30,
  },
  startGameButton: {
    marginTop: '15%',
    width: 200,
    marginLeft: '7%',
    height: 40,
    backgroundColor: '#8D3E8F',
    borderRadius: 10,
    alignItems: 'center',
    paddingTop: '1.8%',
  },
  buttonText: {
    color: '#fff',
    fontSize: 20,
  },
  inputX: {
    fontSize: 20,
    borderBottomColor: '#8D3E8F',
    borderBottomStyle: 'solid',
    borderBottomWidth: 2,
    minWidth: 100,
    marginRight: 30,
  },
  inputO: {
    fontSize: 20,
    borderBottomColor: '#8D3E8F',
    borderBottomStyle: 'solid',
    borderBottomWidth: 2,
    minWidth: 100,
  },
  buttonStyle: {
    textAlign: 'center',
    alignItems: 'center',
    marginBottom: '5%',
    width: 200,
    marginLeft: '7%',
    minHeight: 40,
    backgroundColor: '#8D3E8F',
    borderRadius: 10,
    paddingTop: '1.8%',
    marginTop:'5%',
  },
});

const winLines = [
  [0, 1, 2],
  [3, 4, 5],
  [6, 7, 8],
  [0, 3, 6],
  [1, 4, 7],
  [2, 5, 8],
  [0, 4, 8],
  [2, 4, 6],
];
