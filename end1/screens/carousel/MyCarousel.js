import React, { useRef, useState, useEffect } from 'react';
import Carousel, { ParallaxImage } from 'react-native-snap-carousel';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
  Platform,
  SafeAreaView,
} from 'react-native';

const ENTRIES1 = [
  {
    subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
    illustration: 'https://media.istockphoto.com/vectors/tic-tac-toe-game-with-cross-and-red-heart-sign-vector-id482763076?k=6&m=482763076&s=612x612&w=0&h=OVhHPkXh94hch8xbuHuOcfZbaSittC2M0UBxLvfzoDU=',
  },
  {
    subtitle: 'Lorem ipsum dolor sit amet',
    illustration: 'https://media.istockphoto.com/vectors/tic-tac-toe-icon-filled-tic-tac-toe-icon-for-website-design-and-app-vector-id1167439600?k=6&m=1167439600&s=612x612&w=0&h=GtZfWB2Gv5UByIUuipT4l6UE-X4FVsDgg3TAyYsLQ3Q=',
  },
  {
    subtitle: 'Lorem ipsum dolor sit amet et nuncat ',
    illustration: 'https://media.istockphoto.com/vectors/noughts-and-crosses-vector-id467302805?k=6&m=467302805&s=612x612&w=0&h=02QitjDJSlKCs3bo-rBW-L5tz4w_B6bb2YkKTR_naT0=',
  },
  {
    subtitle: 'Lorem ipsum dolor sit amet et nuncat mergitur',
    illustration: 'https://media.istockphoto.com/vectors/game-tic-tac-toe-icon-vector-outline-illustration-vector-id1214244443?k=6&m=1214244443&s=612x612&w=0&h=-y4F3-SEB_JXa9B1yWx0ajzfX01kqRH-8qtwHjRWALM=',
  },
  {
    subtitle: 'Lorem ipsum dolor sit amet',
    illustration: 'https://media.istockphoto.com/vectors/tic-tac-toe-game-flat-design-icon-vector-id1146577964?k=6&m=1146577964&s=612x612&w=0&h=xo74d5AiwunyDwYqNy5E9I3prXVRco0szs7kKyIuOA8=',
  },
];
const { width: screenWidth } = Dimensions.get('window');

const MyCarousel = (props) => {
  const [entries, setEntries] = useState([]);
  const carouselRef = useRef(null);

  const goForward = () => {
    carouselRef.current.snapToNext();
  };

  useEffect(() => {
    setEntries(ENTRIES1);
  }, []);

  const renderItem = ({ item, index }, parallaxProps) => {
    return (
      <View style={styles.item}>
        <ParallaxImage
          source={{ uri: item.illustration }}
          containerStyle={styles.imageContainer}
          style={styles.image}
          parallaxFactor={0.5}
          {...parallaxProps}
        />
      </View>
    );
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity onPress={goForward} style={styles.buttonStyle}>
        <Text style={styles.buttonNext}>NEXT</Text>
      </TouchableOpacity>
      <Carousel
        ref={carouselRef}
        sliderWidth={screenWidth}
        sliderHeight={screenWidth}
        itemWidth={screenWidth - 60}
        data={entries}
        renderItem={renderItem}
        hasParallaxImages={true}
      />
    </View>
  );
};

export default MyCarousel;

const styles = StyleSheet.create({
  container: {
    marginTop: '10%',
    marginBottom: '5%',
    flex: 0.7,
    backgroundColor: '#fff',
  },
  item: {
    width: screenWidth - 70,
    height: screenWidth - 70,
    backgroundColor: '#fff',
  },
  imageContainer: {
    flex: 1.0,
    marginBottom: Platform.select({ ios: 2, android: 1 }),
    backgroundColor: 'white',
    borderRadius: 8,
  },
  image: {
    ...StyleSheet.absoluteFillObject,
    resizeMode: 'cover',
  },
  buttonStyle: {
    marginBottom: '5%',
    width: 100,
    marginLeft: '7%',
    height: 30,
    backgroundColor: '#8D3E8F',
    borderRadius: 10,
    alignItems: 'center',
    paddingTop: '1.8%',
  },
  buttonNext: {
    fontSize: 16,
    color: 'white',
  },
});
