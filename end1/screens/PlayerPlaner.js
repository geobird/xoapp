import * as React from 'react';
import {Image,Text,View,StyleSheet,ScrollView,SafeAreaView,TouchableOpacity,TextInput,TouchableHighlight,Modal,ImageBackground} from 'react-native';
import { Icon } from 'react-native-elements';
import HeaderItem from '../navigation/HeaderItem';
import xoBackground from './TicTacToe/backgroundCol.png';
import Home from './Home';

export default class PlayerPlaner extends React.Component {
  render() {
    let {navigation} = this.props;
  
    return (
      
      <View style={{backgroundColor:'#fff', textAlign: 'center',
    alignItems: 'center',}}>
        <Text style={styles.congratulationsText}>Congratulations</Text>
        <Text style={styles.winnerName}>Name</Text>
        <Text style={styles.congratulationsText}>is a winner</Text>
        <TouchableOpacity style={styles.buttonStyle}><Text style={styles.buttonText}>Home</Text></TouchableOpacity>
        <TouchableOpacity style={styles.buttonStyle} onPress={() =>  navigation.navigate("Game")}><Text style={styles.buttonText}>New game</Text></TouchableOpacity>
      </View>
      
    );
  }
}
const styles = StyleSheet.create({
  congratulationsText:{
    fontSize:20,
    color:'#8D3E8F',
    marginBottom:20,
    fontWeight:'bold'
  },
  
winnerName:{
  fontSize:30,
  color:'#8D3E8F',
  marginBottom:20,
},
  buttonStyle: {
    textAlign: 'center',
    alignItems: 'center',
    marginBottom: '5%',
    width: 200,
    marginLeft: '7%',
    minHeight: 40,
    backgroundColor: '#8D3E8F',
    borderRadius: 10,
    paddingTop: '1.8%',
    marginTop:'5%',
  },
 buttonText:{
   color:'#fff',
   fontSize:20
 },
  
});

