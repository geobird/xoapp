import * as React from 'react';
import {StyleSheet,  View,  ScrollView,  Text,TouchableOpacity,SafeAreaView, TextInput,ImageBackground} from 'react-native';
import HeaderItem from '../navigation/HeaderItem';
import update from 'immutability-helper';
import GamePanel from './TicTacToe/Game';
import xoBackground from './TicTacToe/backgroundCol.png';



export default class Game extends React.Component {
  render() {
    let {navigation} = this.props;
    return (
      <>
      <ImageBackground
              source={xoBackground}
              style={styles.backgroundStyle}>
      <View style={styles.topSpace}>
        <HeaderItem navigation={navigation} title={"Game"}/>
      </View>
      <View style={styles.container}>
       <GamePanel/>
      </View>
      </ImageBackground>
     </>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 0.8,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop:40
  },
  topSpace:{
    marginTop:40,
    backgroundColor:''
  },
  backgroundStyle: {
    width: '100%',
    height: '100%',
  },
});




