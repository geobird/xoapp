import HeaderItem from '../navigation/HeaderItem';
import React  from 'react';
import {Image,Text,View,StyleSheet,ScrollView,SafeAreaView,TouchableOpacity} from 'react-native';


export default class Window extends React.Component {
  render(props) {
    let {navigation} = this.props;
  
    return (
      <View> 
      <HeaderItem navigation={navigation} title={"Window"}/>
        <Text>
          Error message: {this.state.error}
        </Text>
      </View>
    );
    }
    }