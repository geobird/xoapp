import AsyncStorage from '@react-native-async-storage/async-storage';

export const setDataAsync = async (nickNameX,nickNameO,time,winner,date) => {
  try {
    await AsyncStorage.setItem(nickNameX,nickNameO,time,winner,date)
  } catch (e) {
    console.log("Blad");
  }
}
export const getDataAsync = async (nickNameX,nickNameO,time,winner,date) => {
  try {
    const value = await AsyncStorage.getItem(nickNameX,nickNameO,time,winner,date)
    if(value !== null) {
      return value;
    }
  } catch(e) {
    return false;
  }
}
