import React  from 'react';
import { Image,Text, View, StyleSheet, ScrollView,SafeAreaView,TouchableOpacity,TextInput,Dimensions,Platform} from 'react-native';
import {Icon} from 'react-native-elements';
import HeaderItem from '../navigation/HeaderItem';
import Carousel, {ParallaxImage} from 'react-native-snap-carousel';
import MyCarousel from './carousel/MyCarousel.js';

const {width: screenWidth} = Dimensions.get('window');

export default class Instruction extends React.Component {
   render(){
   let {navigation} = this.props;
    return (
      <View style={{flex:1, backgroundColor:'#fff'}}>
      <View style={{marginTop:40, flex:1}}>
        <HeaderItem navigation={navigation} title={"Instruction"}/>
         <MyCarousel/>
        <View style={{alignItems:'center',  justifyContent:'center'}}> 
          <Text style={{fontSize: 20,fontStyle:'oblique',color:'#8D3E8F'}}>Tic Tac Toe </Text>
          <Text style={{fontSize: 20,fontStyle:'oblique',color:'#8D3E8F'}}>Instruction</Text>
          <Text style={{  fontSize: 16, fontStyle:'normal', marginLeft: 20, marginRight: 20, color:'#8D3E8F'}}>Players alternate between squares, trying to cover three squares in one line, while preventing the opponent from doing the same. A field may be occupied by one player and does not change its owner throughout the game. </Text>
          </View>
          </View>
        </View>
  
         );
  }
} 


 