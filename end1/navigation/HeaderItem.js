import {StatusBar, StyleSheet, Text, View,TouchableOpacity, Image} from "react-native";
import { Button } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import * as React from "react";

function HeaderItem(props) {
    let {navigation, title} = props;
    return (
        <View style={styles.header}>
          <TouchableOpacity style={styles.headerButton} onPress={()=> navigation.toggleDrawer()}>
            <View style={styles.lines}>
             <Icon name="bars" size={30} color='#8D3E8F'/>
            </View>
          </TouchableOpacity>
            <Text style={styles.headerText}>{title}</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    header: {
        
        backgroundColor: 'transparent',
        borderBottomColor: 'white',
        borderStyle: 'solid',
        borderBottomWidth: 2,
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },

    headerButton: {
        flex: 1,
        paddingTop:5,
    },

    headerText: {
        flex: 1.6,
        textAlign: "center",
        fontSize: 25,
        color: '#8D3E8F',
        paddingBottom: 10,
        paddingTop: 10,
        paddingRight: 90,
      //  paddingLeft: 50,
    },
    lines:{
      paddingBottom:5,
      alignItems:'center'
    }

  
});

export default HeaderItem;