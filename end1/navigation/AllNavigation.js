import 'react-native-gesture-handler';
import * as React from 'react';
import { View, TouchableOpacity, Image,StyleSheet } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createDrawerNavigator } from '@react-navigation/drawer';
import Home from '../screens/Home';
import Game from '../screens/Game';
import Result from '../screens/Result';

//import TestNumber from '../screens/TestNumber';
import CustomSidebarMenu from './CustomSidebarMenu';
import Instruction from '../screens/Instruction';
import { AntDesign } from '@expo/vector-icons';
import { Foundation } from '@expo/vector-icons';
import { MaterialIcons } from '@expo/vector-icons';
import { Ionicons } from '@expo/vector-icons';
import { Entypo } from '@expo/vector-icons';


const Drawer = createDrawerNavigator();
 
const NavigationDrawerStructure = (props) => {
  const toggleDrawer = () => {
    props.navigationProps.toggleDrawer();
  };
  
   
  return (
    <Drawer.Navigator drawerContent={props => <CustomSidebarMenu {...props} />} drawerContentOptions={{
        activeTintColor: '#fff',
        inactiveTintColor:'#fff',
        itemStyle: {color:'#fff'},
        width:280,
      }} drawerStyle={{width: 300,  }}> 
       <Drawer.Screen name="Home" component={Home} options={{ title: "Home", unmountOnBlur: true, drawerIcon: config => <Ionicons name="home-outline" size={30} color="white" />}} unmountOnBlur={true}/>
        <Drawer.Screen name="Game" component={Game} options={{ title: "Game", unmountOnBlur: true, drawerIcon: config =><Ionicons name="game-controller-outline" size={30} color="white" />}} unmountOnBlur={true}/>
            <Drawer.Screen name="Result" component={Result} options={{ title: "Result", unmountOnBlur: true, drawerIcon: config =><AntDesign name="barschart" size={30} color="white" />}} unmountOnBlur={true}/>
            <Drawer.Screen name="Instruction" component={Instruction} options={{ title: "Instruction", unmountOnBlur: true, drawerIcon: config =><AntDesign name="infocirlceo" size={30} color="white" />}} unmountOnBlur={true}/>
    </Drawer.Navigator>
  );
};

export default class AllNavigation extends React.Component{
  
  render(){
    
return(
    <NavigationContainer>
      <NavigationDrawerStructure/>
    </NavigationContainer>
  );
}
}
