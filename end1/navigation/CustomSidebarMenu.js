import React from 'react';
import {SafeAreaView,View,StyleSheet,Image,Text,Linking,TouchableOpacity,Pressable} from 'react-native';
import {DrawerContentScrollView,DrawerItemList,DrawerItem,} from '@react-navigation/drawer';
import imageIm from '../assets/imageIm.jpg';
import { Icon } from 'react-native-elements'
import _ from 'lodash';
import tlo from '../assets/tloKK2.png';
import Gradient from 'react-native-css-gradient';


const gradient = 'linear-gradient(to bottom, #923993 0%,   #2C1D66 100%)';


class CustomSidebarMenu extends React.Component {

  render(){
  return (
     <Gradient gradient={gradient} style={{width:300,
                  height:'100%',
                  alignItems: "flex-start",
                  justifyContent: "flex-end",
                  padding: 20,
                  marginBottom: 4}}>
    <SafeAreaView style={styles.container}>
     
     <Text style={{fontWeight:"bold", textAlign:'center', marginTop:20, color:'#fff',fontSize:30}}>XOApp</Text>
      <DrawerContentScrollView {...this.props}  >
        <DrawerItemList {...this.props} drawerContentOptions={{
        activeTintColor: '#fff', inactiveTintColor:'#fff', fontSize:20}}/>
      </DrawerContentScrollView>
     
    </SafeAreaView>
 </Gradient>
  );
}
}

const styles = StyleSheet.create({
  sideMenuProfileIcon: {
    resizeMode: 'center',
    width: 250,
    height: 250,
    borderRadius: 50,
    alignSelf: 'center',
  },
   container: {
    flex: 1,  
    backgroundColor: 'transpatent'
  },


});

export default CustomSidebarMenu;